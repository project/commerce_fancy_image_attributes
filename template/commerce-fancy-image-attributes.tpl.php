<?php
/**
 * @file
 * Default implementation of a commerce fancy image attribute.
 *
 * Available variables:
 * - $count: The amount of found items
 * - $title: The title of the facet
 * - $image: The image to display.
 *
 * Helper variables:
 * - $display_count: If the count should be displayed or not.
 * - $display_title: If the title should be displayed or not.
 *
 * @see template_preprocess()
 * @see template_process()
 */
?>

<span class="commerce-fancy-image-attribute">
<?php print $image; ?>

<?php if ($display_title): ?>
  <span class="commerce-fancy-image-attribute-title"><?php print $title; ?></span>
<?php endif; ?>

<?php if ($display_count): ?>
  <span class="commerce-fancy-image-attribute-count">(<?php print $count; ?>)</span>
<?php endif; ?>
</span>
